# @hashicorp/react-text-split-with-code

## 3.3.8

### Patch Changes

- Updated dependencies [[`14cf3ad`](https://github.com/hashicorp/react-components/commit/14cf3ad2c8f20adfa1c50971f3646f66537a778b)]:
  - @hashicorp/react-text-split@4.0.0
