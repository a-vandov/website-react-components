# @hashicorp/react-text-split-with-image

## 4.2.5

### Patch Changes

- Updated dependencies [[`14cf3ad`](https://github.com/hashicorp/react-components/commit/14cf3ad2c8f20adfa1c50971f3646f66537a778b)]:
  - @hashicorp/react-text-split@4.0.0
