import LinkWrap from '@hashicorp/react-link-wrap'
import InlineSvg from '@hashicorp/react-inline-svg'
import s from './style.module.css'
import classNames from 'classnames'
/* main logos, for light theme */
import ArbiterLogo from './logo.svg?include'
import DoxtreamLogo from './doxtream.svg?include'

const logoDict = {
  arbiter: ArbiterLogo,
  doxtream: DoxtreamLogo,
}

function TitleLink({ text, url, product, Link, theme }) {
  const Logo = logoDict[text]
  return (
    <LinkWrap
      Link={Link}
      className={classNames(s.root, s[`brand-${product}`])}
      href={url}
      title={text}
    >
      {<InlineSvg src={Logo} />}
    </LinkWrap>
  )
}

export default TitleLink
