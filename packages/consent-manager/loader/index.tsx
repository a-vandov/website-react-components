import React from 'react'
import ConsentManager, { open } from '../'
import defaultCategories from './categories'
import ossPreset from './presets/oss'
import enterprisePreset from './presets/enterprise'
import {
  ConsentManagerCategory,
  ConsentManagerConfig,
  ConsentManagerService,
} from './types'

export default function createConsentManager({
  segmentWriteKey = process.env.SEGMENT_WRITE_KEY,
  preset,
  segmentServices,
  otherServices,
  categories,
  forceShow = false,
}: {
  segmentWriteKey?: string
  preset?: 'oss' | 'enterprise'
  segmentServices?: ConsentManagerService[]
  otherServices?: ConsentManagerService[]
  categories?: ConsentManagerCategory[]
  forceShow?: boolean
}): { ConsentManager: React.ComponentType; openConsentManager: () => void } {
  // if arbiter env is present, check against it. if not, fall back to checking node env
  const isProd = process.env.ARB_ENV
    ? process.env.ARB_ENV === 'production'
    : process.env.NODE_ENV === 'production'

  if (!process.env.ARB_ENV)
    console.warn(
      'Consent manager expects the "ARB_ENV" environment variable to be set in order to ensure proper analytics tracking. Please make sure "ARB_ENV" is set appropriately within your app.'
    )

  // set the correct segment key based on environment
  const segmentKey = isProd
    ? segmentWriteKey
    : 'CTZWcQLPsVTHpKkrbrRrUL9GfgDSBgkw'

  // next we build the config objct, kicking it off with the default values
  let config: ConsentManagerConfig = {
    version: 4,
    companyName: 'Arbiter',
    privacyPolicyLink: 'http://arbiterproject.io/legal/privacy',
    cookiePolicyLink: 'http://arbiterproject.io/legal/cookie',
    segmentWriteKey: segmentKey,
    categories: defaultCategories,
    forceShow,
  }

  // add preset values if present
  if (preset === 'oss') config = { ...config, ...ossPreset }
  if (preset === 'enterprise') config = { ...config, ...enterprisePreset }

  // add any other custom values nondestructively if present
  if (segmentServices) {
    if (!config.segmentServices) config.segmentServices = []
    config.segmentServices.push(...segmentServices)
  }
  if (otherServices) {
    if (!config.additionalServices) config.additionalServices = []
    config.additionalServices.push(...otherServices)
  }
  if (categories) config.categories.push(...categories)

  // finally, we return a HOC that will render the fully configured consent manager
  return {
    ConsentManager: function ConsentManagerWrapper() {
      return <ConsentManager {...config} />
    },
    openConsentManager: open,
  }
}
