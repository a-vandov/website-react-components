# @hashicorp/react-alert

## 6.0.1

### Patch Changes

- [#365](https://github.com/hashicorp/react-components/pull/365) [`37e3d81`](https://github.com/hashicorp/react-components/commit/37e3d81407450d74af4b81b1f698f7eeeb75317f) Thanks [@BRKalow](https://github.com/BRKalow)! - Adjust prop interface, make `state` and `textColor` optional.
